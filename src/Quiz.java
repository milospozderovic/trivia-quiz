import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class Quiz implements ActionListener {

    String[] questions = {
            "What is the world's largest country by land area?",
            "What is the largest mammal in the world?",
            "In which country is the Great Barrier Reef located?",
            "Who painted the famous artwork, \"The Starry Night\"?",
            "Which planet in our solar system is known as the \"Red Planet\"?",
            "Which country won the first FIFA World Cup in 1930?",
            "Who wrote the novel \"The Catcher in the Rye\"?",
            "What is the highest mountain in Africa?",
            "Who directed the movie \"Jurassic Park\"?",
            "Which city is considered the fashion capital of the world?",
            "Which American state is known as the \"Sunshine State\"?",
            "Who painted the famous artwork \"The Mona Lisa\"?",
            "What is the capital city of Spain?",
            "What is the smallest planet in our solar system?",
            "Who played the character of Harry Potter in the Harry Potter movie series?",
            "What is the currency of Japan?",
            "Which river is the longest in the world?",
            "Who is the lead singer of the rock band U2?",
            "Which country is both a continent and an island?",
            "Who wrote the play \"Hamlet\"?",
            "Which country is the largest producer of coffee in the world?",
            "What is the capital of Canada?",
            "Who is the creator of the television series \"The Office\"?",
            "In which year did the Titanic sink?",
            "Which river runs through Paris, France?",
            "Which country was the first to send a man to the moon?",
            "Who is the author of the Harry Potter book series?",
            "What is the largest animal in the world?",
            "Which country is known as the \"Land of the Rising Sun\"?",
            "Who was the first female prime minister of the United Kingdom?",
            "What is the capital city of Brazil?",
            "What is the highest mountain in North America?",
            "Which country is known as the \"Land of a Thousand Lakes\"?",
            "Who was the first U.S. president to be impeached?",
            "What is the smallest country in the world?",
            "Who is the author of the \"Lord of the Rings\" book series?",
            "Which country is the largest in South America?",
            "Who painted the famous artwork \"The Last Supper\"?",
            "Who wrote the novel \"To Kill a Mockingbird\"?",
            "Which planet in our solar system has the most moons?",
            "Who was the first person to reach the South Pole?",
            "Which country is the largest island in the world?",
            "What is the smallest bone in the human body?",
            "Who invented the telephone?",
            "What is the largest organ in the human body?",
            "Who painted the famous artwork \"Mona Lisa\"?",
            "What is the most widely spoken language in the world?",
            "What is the name of the largest ocean on Earth?",
            "Who wrote the novel \"1984\"?",
            "Which gas makes up the majority of Earth's atmosphere?"

    };

    String[][] options = {
            {"China", "Russia","Canada","United States"},
            {"Elephant", "Blue whale","Giraffe","Hippopotamus"},
            {"Australia", "Fiji","Indonesia","Philippines"},
            {"Leonardo da Vinci", "Michelangelo","Vincent van Gogh","Pablo Picasso"},
            {"Venus","Mars","Jupiter","Saturn"},
            {"Brazil","Argentina","Uruguay","Italy"},
            {"J.K.Rowling","F.Scott Fitzgerald","Ernest Hemingway","J.D.Salinger"},
            {"Mount Kilimanjaro","Mount Everest","Mount Fuji","Mount Denali"},
            {"Steven Spielberg","George Lucas","Quentin Tarantino","Martin Scorsese"},
            {"Paris","Milan","New York","London"},
            {"California","Florida","Texas","Arizona"},
            {"Leonardo da Vinci","Michelangelo","Vincent van Gogh","Pablo Picasso"},
            {"Paris","Madrid","Rome","London"},
            {"Mercury","Venus","Mars","Neptune"},
            {"Rupert Grint","Daniel Radcliffe","Tom Felton","Emma Watson"},
            {"Dollar","Euro","Yen","Pound"},
            {"Nile","Amazon","Missisippi","Yangtze"},
            {"Bono","Mick Jagger","Freddie Mercury","Bruce Springsteen"},
            {"Australia","Madagascar","Greenland","Japan"},
            {"William Shakespeare","Samuel Beckett","Tennessee Williams","Arthur Miller"},
            {"Brazil","Colombia","Vietnam","Ethiopia"},
            {"Ottawa","Toronto","Vancouver","Montreal"},
            {"Ricky Gervais","Steve Carell","Mindy Kaling","John Krasinski"},
            {"1907","1912","1923","1931"},
            {"Rhine","Seine","Danube","Thames"},
            {"United States","Russia","China","Japan"},
            {"J.K.Rowling","Stephenie Meyer","Suzanne Collins","George R.R. Martin"},
            {"African elephant","Blue whale","Killer whale","Giant squid"},
            {"China","Japan","Thailand","Vietnam"},
            {"Margaret Thatcher","Theresa May","Angela Merkel","Jacinda Ardern"},
            {"Rio de Janeiro","Brasilia","Sao Paulo","Salvador"},
            {"Mount Denali","Mount Whitney","Mount Rainier","Mount Shasta"},
            {"Finland","Sweden","Norway","Denmark"},
            {"Andrew Johnson","Bill Clinton","Richard Nixon","Donald Trump"},
            {"Monaco","San Marino","Vatican City","Liechtenstein"},
            {"J.R.R. Tolkien","C.S. Lewis","George R.R. Martin","Stephen King"},
            {"Argentina","Brazil","Chile","Peru"},
            {"Leonardo da Vinci","Michelangelo","Pablo Picasso","Vincent van Gogh"},
            {"Harper Lee","John Steinbeck","Ernest Hemingway","F.Scott Fitzgerald"},
            {"Jupiter","Saturn","Uranus","Neptune"},
            {"Roald Amundsen","Robert Falcon Scott","Ernest Shackleton","Edmund Hillary"},
            {"Greenland","Australia","Madagascar","Cuba"},
            {"Stapes (in the ear)","Patella (kneecap)","Femur (thigh bone)","Tibia (shin bone)"},
            {"Alexander Graham Bell","Thomas Edison","Nikola Tesla","Guglielmo Marconi"},
            {"Brain","Liver","Heart","Skin"},
            {"Pablo Picasso","Michelangelo","Leonardo da Vinci","Raphael"},
            {"English","Spanish","Mandarin Chinese","French"},
            {"Pacific Ocean","Atlantic Ocean","Indian Ocean","Southern Ocean"},
            {"George Orwell","Aldous Huxley","Ray Bradbury","J.D. Salinger"},
            {"Oxygen","Nitrogen","Carbon dioxide","Helium"}


    };

    char[] answers = {
            'B',
            'B',
            'A',
            'C',
            'B',
            'C',
            'D',
            'A',
            'A',
            'B',
            'B',
            'A',
            'B',
            'A',
            'B',
            'C',
            'A',
            'A',
            'A',
            'A',
            'A',
            'A',
            'A',
            'B',
            'B',
            'A',
            'A',
            'B',
            'B',
            'A',
            'B',
            'A',
            'A',
            'A',
            'C',
            'A',
            'B',
            'A',
            'A',
            'A',
            'A',
            'A',
            'A',
            'A',
            'D',
            'C',
            'C',
            'A',
            'A',
            'B'
    };

    char guess;
    char answer;
    int index;
    int correct_guesses = 0;
    int total_questions = questions.length;
    int result;
    int seconds = 20;

    JFrame frame = new JFrame();
    JTextField textField = new JTextField();
    JTextArea textArea = new JTextArea();
    JButton buttonA = new JButton();
    JButton buttonB = new JButton();
    JButton buttonC = new JButton();
    JButton buttonD = new JButton();
    JLabel answer_labelA = new JLabel();
    JLabel answer_labelB = new JLabel();
    JLabel answer_labelC = new JLabel();
    JLabel answer_labelD = new JLabel();
    JLabel time_label = new JLabel();
    JLabel seconds_left = new JLabel();
    JTextField number_right = new JTextField();
    JTextField percentage = new JTextField();

    Timer timer = new Timer(1000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            seconds--;
            seconds_left.setText(String.valueOf(seconds));
            if (seconds<=0) {
                displayAnswer();
            }
        }
    });

    public Quiz() {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(650,650);
        frame.getContentPane().setBackground(new Color(50,50,50));
        frame.setLayout(null);
        frame.setResizable(false);

        textField.setBounds(0,0,650,50);
        textField.setBackground(new Color(25,25,25));
        textField.setForeground(new Color(25,255,0));
        textField.setFont(new Font("Ink Free",Font.BOLD,30));
        textField.setBorder(BorderFactory.createBevelBorder(1));
        textField.setHorizontalAlignment(JTextField.CENTER);
        textField.setEditable(false);

        textArea.setBounds(0,50,650,50);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setBackground(new Color(25,25,25));
        textArea.setForeground(new Color(25,255,0));
        textArea.setFont(new Font("MV Boli",Font.BOLD,25));
        textArea.setBorder(BorderFactory.createBevelBorder(1));
        textArea.setEditable(false);

        buttonA.setBounds(0,100,100,100);
        buttonA.setFont(new Font("MV Boli",Font.BOLD,35));
        buttonA.setFocusable(false);
        buttonA.addActionListener(this);
        buttonA.setText("A");

        buttonB.setBounds(0,200,100,100);
        buttonB.setFont(new Font("MV Boli",Font.BOLD,35));
        buttonB.setFocusable(false);
        buttonB.addActionListener(this);
        buttonB.setText("B");

        buttonC.setBounds(0,300,100,100);
        buttonC.setFont(new Font("MV Boli",Font.BOLD,35));
        buttonC.setFocusable(false);
        buttonC.addActionListener(this);
        buttonC.setText("C");

        buttonD.setBounds(0,400,100,100);
        buttonD.setFont(new Font("MV Boli",Font.BOLD,35));
        buttonD.setFocusable(false);
        buttonD.addActionListener(this);
        buttonD.setText("D");

        answer_labelA.setBounds(125,100,500,100);
        answer_labelA.setBackground(new Color(50,50,50));
        answer_labelA.setForeground(new Color(25,255,0));
        answer_labelA.setFont(new Font("MV Boli",Font.PLAIN,35));

        answer_labelB.setBounds(125,200,500,100);
        answer_labelB.setBackground(new Color(50,50,50));
        answer_labelB.setForeground(new Color(25,255,0));
        answer_labelB.setFont(new Font("MV Boli",Font.PLAIN,35));

        answer_labelC.setBounds(125,300,500,100);
        answer_labelC.setBackground(new Color(50,50,50));
        answer_labelC.setForeground(new Color(25,255,0));
        answer_labelC.setFont(new Font("MV Boli",Font.PLAIN,35));

        answer_labelD.setBounds(125,400,500,100);
        answer_labelD.setBackground(new Color(50,50,50));
        answer_labelD.setForeground(new Color(25,255,0));
        answer_labelD.setFont(new Font("MV Boli",Font.PLAIN,35));

        seconds_left.setBounds(535,510,100,100);
        seconds_left.setBackground(new Color(25,25,25));
        seconds_left.setForeground(new Color(255,0,0));
        seconds_left.setFont(new Font("Ink Free",Font.BOLD,60));
        seconds_left.setBorder(BorderFactory.createBevelBorder(1));
        seconds_left.setOpaque(true);
        seconds_left.setHorizontalAlignment(JTextField.CENTER);
        seconds_left.setText(String.valueOf(seconds));

        time_label.setBounds(535,475,100,25);
        time_label.setBackground(new Color(50,50,50));
        time_label.setForeground(new Color(255,0,0));
        time_label.setFont(new Font("MV Boli",Font.PLAIN,17));
        time_label.setHorizontalAlignment(JTextField.CENTER);
        time_label.setText("Timer");

        number_right.setBounds(225,225,200,100);
        number_right.setBackground(new Color(25,25,25));
        number_right.setForeground(new Color(25,255,0));
        number_right.setFont(new Font("Ink Free",Font.BOLD,50));
        number_right.setBorder(BorderFactory.createBevelBorder(1));
        number_right.setHorizontalAlignment(JTextField.CENTER);
        number_right.setEditable(false);

        percentage.setBounds(225,325,200,100);
        percentage.setBackground(new Color(25,25,25));
        percentage.setForeground(new Color(25,255,0));
        percentage.setFont(new Font("Ink Free",Font.BOLD,50));
        percentage.setBorder(BorderFactory.createBevelBorder(1));
        percentage.setHorizontalAlignment(JTextField.CENTER);
        percentage.setEditable(false);


        frame.add(time_label);
        frame.add(seconds_left);
        frame.add(answer_labelA);
        frame.add(answer_labelB);
        frame.add(answer_labelC);
        frame.add(answer_labelD);
        frame.add(buttonA);
        frame.add(buttonB);
        frame.add(buttonC);
        frame.add(buttonD);
        frame.add(textArea);
        frame.add(textField);

        frame.setVisible(true);

        nextQuestion();
    }

    public void nextQuestion() {
        if (index>=total_questions) {
            results();
        } else {
            textField.setText("Question "+(index+1));
            textArea.setText(questions[index]);
            answer_labelA.setText(options[index][0]);
            answer_labelB.setText(options[index][1]);
            answer_labelC.setText(options[index][2]);
            answer_labelD.setText(options[index][3]);
            timer.start();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        buttonA.setEnabled(false);
        buttonB.setEnabled(false);
        buttonC.setEnabled(false);
        buttonD.setEnabled(false);

        if (e.getSource()==buttonA) {
            answer = 'A';
            if (answer == answers[index]) {
                correct_guesses++;
            }
        }
        if (e.getSource()==buttonB) {
            answer = 'B';
            if (answer == answers[index]) {
                correct_guesses++;
            }
        }
        if (e.getSource()==buttonC) {
            answer = 'C';
            if (answer == answers[index]) {
                correct_guesses++;
            }
        }
        if (e.getSource()==buttonD) {
            answer = 'D';
            if (answer == answers[index]) {
                correct_guesses++;
            }
        }

        displayAnswer();
    }

    public void displayAnswer() {

        timer.stop();
        buttonA.setEnabled(false);
        buttonB.setEnabled(false);
        buttonC.setEnabled(false);
        buttonD.setEnabled(false);

        if (answers[index] != 'A')
            answer_labelA.setForeground(new Color(255, 0, 0));
        if (answers[index] != 'B')
            answer_labelB.setForeground(new Color(255, 0, 0));
        if (answers[index] != 'C')
            answer_labelC.setForeground(new Color(255, 0, 0));
        if (answers[index] != 'D')
            answer_labelD.setForeground(new Color(255, 0, 0));

        Timer pause = new Timer(2000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                answer_labelA.setForeground(new Color(25, 255, 0));
                answer_labelB.setForeground(new Color(25, 255, 0));
                answer_labelC.setForeground(new Color(25, 255, 0));
                answer_labelD.setForeground(new Color(25, 255, 0));

                answer = ' ';
                seconds = 20;
                seconds_left.setText(String.valueOf(seconds));
                buttonA.setEnabled(true);
                buttonB.setEnabled(true);
                buttonC.setEnabled(true);
                buttonD.setEnabled(true);
                index++;
                nextQuestion();
            }
        });
        pause.setRepeats(false);
        pause.start();
    }

    public void results() {

        buttonA.setEnabled(false);
        buttonB.setEnabled(false);
        buttonC.setEnabled(false);
        buttonD.setEnabled(false);

        result = (int)((correct_guesses/(double)total_questions)*100);

        textField.setText("Results!");
        textArea.setText("");
        answer_labelA.setText("");
        answer_labelB.setText("");
        answer_labelC.setText("");
        answer_labelD.setText("");

        number_right.setText("(" + correct_guesses + "/" + total_questions + ")");
        percentage.setText(result + "%");

        frame.add(number_right);
        frame.add(percentage);
    }
}
